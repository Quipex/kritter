package com.threadjava.auth;

public class AccountExistsException extends RuntimeException {
	public AccountExistsException() {
	}
	
	public AccountExistsException(String message) {
		super(message);
	}
}
