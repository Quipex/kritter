package com.threadjava.auth;

import com.threadjava.auth.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    private AuthService authService;

    @PostMapping("/register")
    public AuthUserDTO signUp(@RequestBody UserRegisterDto user) throws Exception {
        return authService.register(user);
    }

    @PostMapping("/login")
    public AuthUserDTO login(@RequestBody UserLoginDTO user) throws Exception {
        return authService.login(user);
    }
    
    @PostMapping("resetPassword")
    public void resetPassword(@RequestBody ResetPasswordTokenRequestDto resetPasswordDto) {
        authService.generatePwResetTokenAndSend(resetPasswordDto);
    }
    
    @PostMapping("changePassword")
    public void changePassword(@RequestBody ResetPasswordDto resetPasswordDto) {
        authService.validateTokenAndResetPassword(resetPasswordDto);
    }
    
}
