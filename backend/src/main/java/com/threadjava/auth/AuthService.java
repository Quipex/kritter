package com.threadjava.auth;

import com.threadjava.auth.dto.*;
import com.threadjava.auth.model.AuthUser;
import com.threadjava.auth.model.PasswordResetToken;
import com.threadjava.email.MailService;
import com.threadjava.users.UsersService;
import com.threadjava.users.model.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.UUID;

@Service
@Slf4j
public class AuthService {
	@Autowired
	private PasswordEncoder bCryptPasswordEncoder;
	@Autowired
	private AuthenticationManager authenticationManager;
	@Autowired
	private TokenService tokenService;
	@Autowired
	private UsersService userDetailsService;
	@Autowired
	private PasswordResetTokenRepository passwordResetTokenRepository;
	@Autowired
	private MailService mailService;
	@Value("${application.client.base_url}")
	private String BASE_URL;
	@Value("${application.client.port}")
	private String PORT;
	
	public AuthUserDTO register(UserRegisterDto userDto) throws Exception {
		userDetailsService.findByEmail(userDto.getEmail()).ifPresent(user -> {
			throw new AccountExistsException("Email already taken");
		});
		userDetailsService.findByUsername(userDto.getUsername()).ifPresent(user -> {
			throw new AccountExistsException("Username already taken");
		});
		User user = AuthUserMapper.MAPPER.userRegisterDtoToUser(userDto);
		var loginDTO = new UserLoginDTO(user.getEmail(), user.getPassword());
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		userDetailsService.save(user);
		return login(loginDTO);
	}
	
	public AuthUserDTO login(UserLoginDTO user) throws Exception {
		Authentication auth;
		try {
			auth = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPassword()));
		} catch (BadCredentialsException e) {
			throw new Exception("Incorrect username or password", e);
		}
		
		var currentUser = (AuthUser) auth.getPrincipal();
		final var userDetails = userDetailsService.getUserById(currentUser.getId());
		final String jwt = tokenService.generateToken(currentUser);
		return new AuthUserDTO(jwt, userDetails);
	}
	
	public void generatePwResetTokenAndSend(ResetPasswordTokenRequestDto resetPasswordDto) {
		log.debug("New request on resetting pw: {}", resetPasswordDto);
		var foundUser = userDetailsService.findByEmail(resetPasswordDto.getEmail()).orElseThrow();
//		todo: replace token generation with something more complex
		String token = UUID.randomUUID().toString();
		createPasswordResetTokenForUser(token, foundUser);
		sendMail(foundUser, token);
	}
	
	private void sendMail(User restoredUser, String token) {
		final String url = "http://" + BASE_URL + ":" + PORT + "/change_password?token=" + token + "&email=" + restoredUser.getEmail();
		final String message = "To change your password click on the link: \n" + url + "\n";
		mailService.generateAndSendMail("Reset password Thread SPA", message, restoredUser.getEmail());
	}
	
	private void createPasswordResetTokenForUser(String token, User foundUser) {
		var existedToken = passwordResetTokenRepository.findByUser(foundUser);
		existedToken.ifPresent(passwordResetToken -> {
			log.debug("Old token was present, deleting");
			passwordResetTokenRepository.delete(passwordResetToken);
		});
		PasswordResetToken passwordResetToken = new PasswordResetToken(token, foundUser);
		passwordResetTokenRepository.save(passwordResetToken);
		log.debug("Generated new token");
	}
	
	public void validateTokenAndResetPassword(ResetPasswordDto resetPasswordDto) {
		var resetPasswordToken = passwordResetTokenRepository
				.findByTokenAndUserEmail(resetPasswordDto.getToken(), resetPasswordDto.getEmail()).orElseThrow();
		if (isTokenExpired(resetPasswordToken)) {
			passwordResetTokenRepository.delete(resetPasswordToken);
			throw new PasswordResetTokenException("Token expired");
		} else {
			var user = resetPasswordToken.getUser();
			user.setPassword(bCryptPasswordEncoder.encode(resetPasswordDto.getPassword()));
			log.debug("Changed pw for {}", resetPasswordDto.getEmail());
			userDetailsService.save(user);
			log.debug("Deleting token afterwards");
			passwordResetTokenRepository.delete(resetPasswordToken);
		}
	}
	
	private boolean isTokenExpired(PasswordResetToken token) {
		final Calendar cal = Calendar.getInstance();
		return token.getExpiryDate().before(cal.getTime());
	}
}
