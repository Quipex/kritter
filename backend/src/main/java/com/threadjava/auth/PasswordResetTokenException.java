package com.threadjava.auth;

public class PasswordResetTokenException extends RuntimeException {
	public PasswordResetTokenException() {
	}
	
	public PasswordResetTokenException(String message) {
		super(message);
	}
}
