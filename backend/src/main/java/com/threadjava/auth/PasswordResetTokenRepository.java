package com.threadjava.auth;

import com.threadjava.auth.model.PasswordResetToken;
import com.threadjava.users.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.Optional;

public interface PasswordResetTokenRepository extends JpaRepository<PasswordResetToken, Long> {
	
	Optional<PasswordResetToken> findByTokenAndUserEmail(String token, String email);
	
	@Modifying
	@Query("delete from PasswordResetToken t where t.expiryDate <= ?1")
//	todo: add cron operation that summons this
	void deleteAllExpiredSince(Date now);
	
	Optional<PasswordResetToken> findByUser(User foundUser);
}
