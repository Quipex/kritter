package com.threadjava.auth.dto;

import lombok.Data;

@Data
public class ResetPasswordTokenRequestDto {
	private String email;
}
