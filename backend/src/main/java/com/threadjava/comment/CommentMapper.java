package com.threadjava.comment;

import com.threadjava.comment.dto.CommentDetailsDto;
import com.threadjava.comment.dto.CommentSaveDto;
import com.threadjava.comment.model.Comment;
import com.threadjava.reactions.comments.model.CommentReaction;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.function.Predicate;

@Mapper
public interface CommentMapper {
	CommentMapper MAPPER = Mappers.getMapper(CommentMapper.class);
	
	@Mapping(source = "post.id", target = "postId")
	@Mapping(target = "likeCount", source = "reactions", qualifiedByName = "reactionsToLikes")
	@Mapping(target = "dislikeCount", source = "reactions", qualifiedByName = "reactionsToDislikes")
	CommentDetailsDto commentToCommentDetailsDto(Comment comment);
	
	@Named("reactionsToLikes")
	default long reactionsToLikes(List<CommentReaction> reactions) {
		return reactions.stream().filter(CommentReaction::getIsLike).count();
	}
	
	@Named("reactionsToDislikes")
	default long reactionsToDislikes(List<CommentReaction> reactions) {
		return reactions.stream().filter(Predicate.not(CommentReaction::getIsLike)).count();
	}
	
	@Mapping(source = "postId", target = "post.id")
	@Mapping(source = "userId", target = "user.id")
	@Mapping(target = "id", ignore = true)
	@Mapping(target = "createdAt", ignore = true)
	@Mapping(target = "updatedAt", ignore = true)
	@Mapping(target = "isDeleted", ignore = true)
	@Mapping(target = "reactions", ignore = true)
	Comment commentSaveDtoToModel(CommentSaveDto commentDto);
}
