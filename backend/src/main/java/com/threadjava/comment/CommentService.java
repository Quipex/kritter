package com.threadjava.comment;

import com.threadjava.comment.dto.CommentDetailsDto;
import com.threadjava.comment.dto.CommentSaveDto;
import com.threadjava.comment.dto.CommentUpdateDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class CommentService {
    @Autowired
    private CommentRepository commentRepository;

    public CommentDetailsDto getCommentById(UUID id) {
        return commentRepository.findById(id)
                .map(CommentMapper.MAPPER::commentToCommentDetailsDto)
                .orElseThrow();
    }

    public CommentDetailsDto create(CommentSaveDto commentDto) {
        var comment = CommentMapper.MAPPER.commentSaveDtoToModel(commentDto);
        var postCreated = commentRepository.save(comment);
        return CommentMapper.MAPPER.commentToCommentDetailsDto(postCreated);
    }
    
    public CommentDetailsDto update(CommentUpdateDto commentUpdateDto) {
        var foundComment = commentRepository.findByIdAndUserId(commentUpdateDto.getId(), commentUpdateDto.getUserId())
                .orElseThrow();
        foundComment.setBody(commentUpdateDto.getBody());
        return CommentMapper.MAPPER.commentToCommentDetailsDto(commentRepository.save(foundComment));
    }
    
    public void delete(UUID commentId, UUID userId) {
        var foundComment = commentRepository.findByIdAndUserId(commentId, userId)
                .orElseThrow();
        foundComment.setIsDeleted(true);
        commentRepository.save(foundComment);
    }
}
