package com.threadjava.comment.model;

import com.threadjava.db.BaseEntity;
import com.threadjava.post.model.Post;
import com.threadjava.reactions.comments.model.CommentReaction;
import com.threadjava.reactions.common.Reaction;
import com.threadjava.users.model.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;


@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "comments")
@Where(clause = "is_deleted IS false OR is_deleted IS NULL")
public class Comment extends BaseEntity {
	@Column(name = "body", columnDefinition = "TEXT")
	private String body;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "user_id")
	private User user;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "post_id")
	private Post post;
	
	@OneToMany(mappedBy = "comment", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<CommentReaction> reactions = new ArrayList<>();
	
	@Column(name = "is_deleted")
	private Boolean isDeleted;
	
	@Override
	public String toString() {
		long likes = reactions.stream().filter(Reaction::getIsLike).count();
		long dislikes = reactions.stream().filter(Predicate.not(Reaction::getIsLike)).count();
		return "{\"_super\": { " + super.toString() + " }, " +
				"\"_class\":\"Comment\", " +
				"\"body\":" + (body == null ? "null" : "\"" + body + "\"") + ", " +
				"\"user\":" + (user == null ? "null" : user) + ", " +
				"\"post\":" + (post == null ? "null" : post) + ", " +
				"\"likes\":" + likes + ", " +
				"\"dislikes\":" + dislikes + ", " +
				"\"isDeleted\":" + (isDeleted == null ? "null" : "\"" + isDeleted + "\"") + ", " +
				"}";
	}
}
