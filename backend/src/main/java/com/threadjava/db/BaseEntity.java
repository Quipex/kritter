package com.threadjava.db;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.Date;
import java.util.UUID;

@Data
@MappedSuperclass
public class BaseEntity {
	
	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "id", updatable = false, nullable = false)
	private UUID id;
	
	@Column(name = "`timestamp`")
	@CreationTimestamp
	private Date createdAt;
	
	@Column(name = "updated_on")
	@UpdateTimestamp
	private Date updatedAt;
	
	@Override
	public String toString() {
		return "{\"_class\":\"BaseEntity\", " +
				"\"id\":" + (id == null ? "null" : "\"" + id + "\"") + ", " +
				"\"createdAt\":" + (createdAt == null ? "null" : "\"" + createdAt + "\"") + ", " +
				"\"updatedAt\":" + (updatedAt == null ? "null" : "\"" + updatedAt + "\"") +
				"}";
	}
	
}
