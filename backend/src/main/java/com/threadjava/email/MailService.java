package com.threadjava.email;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class MailService {
	private JavaMailSender mailSender;
	
	@Value("${spring.mail.username}")
	private String SUPPORT_EMAIL;
	
	public MailService(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}
	
	public void sendMail(SimpleMailMessage message) throws MailException {
		log.debug("Sending mail: {}", message);
		mailSender.send(message);
	}
	
	public void generateAndSendMail(String title, String body, String receiverMail) throws MailException {
		sendMail(generateMessage(title, body, receiverMail));
	}
	
	public SimpleMailMessage generateMessage(String title, String body, String receiverMail) {
		final SimpleMailMessage email = new SimpleMailMessage();
		email.setSubject(title);
		email.setText(body);
		email.setTo(receiverMail);
		email.setFrom(SUPPORT_EMAIL);
		return email;
	}
}
