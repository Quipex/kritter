package com.threadjava.errors;

import com.threadjava.auth.AccountExistsException;
import com.threadjava.auth.PasswordResetTokenException;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.fileupload.impl.FileSizeLimitExceededException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

import java.util.NoSuchElementException;

@ControllerAdvice
@Slf4j
public class DefaultExceptionHandler {
	
	@ExceptionHandler({MaxUploadSizeExceededException.class, FileSizeLimitExceededException.class})
	protected ResponseEntity<Object> fileTooBig(RuntimeException ex) {
		return generateResponse(HttpStatus.PAYLOAD_TOO_LARGE, "File is too big", ex.getLocalizedMessage());
	}
	
	@ExceptionHandler(HttpClientErrorException.UnsupportedMediaType.class)
	protected ResponseEntity<Object> invalidUploadedFileType(HttpClientErrorException.UnsupportedMediaType ex) {
		return generateResponse(HttpStatus.UNSUPPORTED_MEDIA_TYPE, "Unsupported image file type", ex.getLocalizedMessage());
	}
	
	@ExceptionHandler(NoSuchElementException.class)
	protected ResponseEntity<Object> noSuchElement(NoSuchElementException ex) {
		return generateResponse(HttpStatus.NOT_FOUND, "Not found", ex.getLocalizedMessage());
	}
	
	@ExceptionHandler(PasswordResetTokenException.class)
	protected ResponseEntity<Object> pwResetToken(PasswordResetTokenException ex) {
		return generateResponse(HttpStatus.FORBIDDEN, "Invalid token", ex.getLocalizedMessage());
	}
	
	@ExceptionHandler(AccountExistsException.class)
	protected ResponseEntity<Object> accExists(AccountExistsException ex) {
		return generateResponse(HttpStatus.CONFLICT, "Invalid credentials", ex.getLocalizedMessage());
	}
	
//	@ExceptionHandler({Exception.class})
//	public ResponseEntity<Object> handleAll(Exception ex) {
//		log.error("An unknown error was handled", ex);
//		return generateResponse(HttpStatus.INTERNAL_SERVER_ERROR, "Error occurred", ex.getLocalizedMessage());
//	}
	
	private ResponseEntity<Object> generateResponse(HttpStatus status, String message, String errorMessage) {
		var apiError = new ApiError(status, message, errorMessage);
		return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
	}
}
