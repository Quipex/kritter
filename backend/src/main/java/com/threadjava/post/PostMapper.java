package com.threadjava.post;

import com.threadjava.comment.model.Comment;
import com.threadjava.image.ImageMapper;
import com.threadjava.post.dto.PostCommentDto;
import com.threadjava.post.dto.PostUserDto;
import com.threadjava.post.dto.create.PostCreationDto;
import com.threadjava.post.dto.create.PostCreationResponseDto;
import com.threadjava.post.dto.query.PostDetailsDto;
import com.threadjava.post.dto.query.PostDetailsQueryResult;
import com.threadjava.post.dto.query.PostListDto;
import com.threadjava.post.dto.update.PostUpdateResponseDto;
import com.threadjava.post.model.Post;
import com.threadjava.reactions.common.Reaction;
import com.threadjava.users.model.User;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.function.Predicate;


@Mapper(uses = { ImageMapper.class })
public abstract class PostMapper {
    public static final PostMapper MAPPER = Mappers.getMapper(PostMapper.class);

    @Mapping(target = "comments", ignore = true)
    public abstract PostDetailsDto postToPostDetailsDto(PostDetailsQueryResult post);

    @Mapping(source = "user.id", target = "userId")
    public abstract PostCreationResponseDto postToPostCreationResponseDto(Post post);
    
    @Mapping(source = "user.id", target = "userId")
    public abstract PostUpdateResponseDto postToPostUpdateResponseDto(Post post);

    @Mapping(source = "imageId", target = "image.id", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_NULL)
    @Mapping(source = "userId", target = "user.id")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "comments", ignore = true)
    @Mapping(target = "reactions", ignore = true)
    @Mapping(target = "isDeleted", ignore = true)
    public abstract Post postDetailsDtoToPost(PostCreationDto postDetailsDto);
    
    @SuppressWarnings("unused")
    @AfterMapping
    public Post doAfterMapping(@MappingTarget Post entity) {
        if (entity != null && entity.getImage().getId() == null) {
            entity.setImage(null);
        }
        return entity;
    }

//    public abstract PostListDto postListToPostListDto(PostListQueryResult model);
    
    @Mapping(target = "likeCount", source = "reactions", qualifiedByName = "reactionsToLikes")
    @Mapping(target = "dislikeCount", source = "reactions", qualifiedByName = "reactionsToDislikes")
    @Mapping(target = "commentCount", expression = "java(post.getComments().stream().count())")
    public abstract PostListDto postToPostListDto(Post post);

//    used by other mappings
    @SuppressWarnings("unused")
    @Mapping(source = "avatar", target = "image")
    public abstract PostUserDto postUserToPostUserDto(User model);

    @Mapping(target = "likeCount", source = "reactions", qualifiedByName = "reactionsToLikes")
    @Mapping(target = "dislikeCount", source = "reactions", qualifiedByName = "reactionsToDislikes")
    public abstract PostCommentDto commentToCommentDto(Comment comment);
    
    @Named("reactionsToLikes")
    public long reactionsToLikes(List<? extends Reaction> reactions) {
        return reactions.stream().filter(Reaction::getIsLike).count();
    }
    
    @Named("reactionsToDislikes")
    public long reactionsToDislikes(List<? extends Reaction> reactions) {
        return reactions.stream().filter(Predicate.not(Reaction::getIsLike)).count();
    }
}
