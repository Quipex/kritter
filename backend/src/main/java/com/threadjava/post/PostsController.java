package com.threadjava.post;


import com.threadjava.post.dto.create.PostCreationDto;
import com.threadjava.post.dto.create.PostCreationResponseDto;
import com.threadjava.post.dto.query.PostDetailsDto;
import com.threadjava.post.dto.query.PostListDto;
import com.threadjava.post.dto.query.PostsRequestDto;
import com.threadjava.post.dto.update.PostUpdateRequestDto;
import com.threadjava.post.dto.update.PostUpdateResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/posts")
public class PostsController {
	@Autowired
	private PostsService postsService;
	@Autowired
	private SimpMessagingTemplate template;
	
	@PostMapping
	public List<PostListDto> get(@RequestBody PostsRequestDto requestDto) {
		requestDto.getFilter().setAuthorId(getUserId());
		return postsService.getAllPosts(requestDto);
	}
	
	@GetMapping("/{id}")
	public PostDetailsDto get(@PathVariable UUID id) {
		return postsService.getPostById(id);
	}
	
	@PostMapping("/create")
	public PostCreationResponseDto post(@RequestBody PostCreationDto postDto) {
		postDto.setUserId(getUserId());
		var item = postsService.create(postDto);
		template.convertAndSend("/topic/new_post", item);
		return item;
	}
	
	@PostMapping("/update")
	public PostUpdateResponseDto update(@RequestBody PostUpdateRequestDto updatedPostDto) {
		return postsService.update(updatedPostDto, getUserId());
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable UUID id) {
		postsService.delete(id, getUserId());
	}
}
