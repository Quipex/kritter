package com.threadjava.post;

import com.threadjava.comment.CommentRepository;
import com.threadjava.image.model.Image;
import com.threadjava.post.dto.create.PostCreationDto;
import com.threadjava.post.dto.create.PostCreationResponseDto;
import com.threadjava.post.dto.query.PostDetailsDto;
import com.threadjava.post.dto.query.PostListDto;
import com.threadjava.post.dto.query.PostsRequestDto;
import com.threadjava.post.dto.update.PostUpdateRequestDto;
import com.threadjava.post.dto.update.PostUpdateResponseDto;
import com.threadjava.post.model.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PostsService {
	@Autowired
	private PostsRepository postsCrudRepository;
	@Autowired
	private CommentRepository commentRepository;
	
	public List<PostListDto> getAllPosts(PostsRequestDto request) {
		var pageable = PageRequest.of(request.getFrom() / request.getCount(), request.getCount(),
				Sort.by("createdAt").descending());
		var filter = request.getFilter();
		Specification<Post> searchSpecification = Specification.where(null);
		if (filter.isShowOnlyLiked()) {
			//noinspection ConstantConditions
			searchSpecification = searchSpecification.and(PostsSpecifications.wasLikedBy(filter.getAuthorId()));
		}
		if (filter.getAuthorsFilter() != null) {
			switch (filter.getAuthorsFilter()) {
				case SHOW_ONLY:
					//noinspection ConstantConditions
					searchSpecification = searchSpecification.and(PostsSpecifications.isAuthorsPost(filter.getAuthorId()));
					break;
				case HIDE:
					//noinspection ConstantConditions
					searchSpecification = searchSpecification.and(PostsSpecifications.notAuthorsPost(filter.getAuthorId()));
					break;
			}
		}
		return postsCrudRepository.findAll(searchSpecification, pageable).stream()
				.map(PostMapper.MAPPER::postToPostListDto).collect(Collectors.toList());
	}
	
	public PostDetailsDto getPostById(UUID id) {
		var post = postsCrudRepository.findPostById(id)
				.map(PostMapper.MAPPER::postToPostDetailsDto)
				.orElseThrow();
		
		var comments = commentRepository.findAllByPostId(id)
				.stream()
				.map(PostMapper.MAPPER::commentToCommentDto)
				.collect(Collectors.toList());
		post.setComments(comments);
		
		return post;
	}
	
	public PostCreationResponseDto create(PostCreationDto postDto) {
		Post post = PostMapper.MAPPER.postDetailsDtoToPost(postDto);
		Post postCreated = postsCrudRepository.save(post);
		return PostMapper.MAPPER.postToPostCreationResponseDto(postCreated);
	}
	
	public PostUpdateResponseDto update(PostUpdateRequestDto updatedPostDto, UUID userId) {
		Post existingPost = postsCrudRepository.findByIdAndUserId(updatedPostDto.getId(), userId)
				.orElseThrow();
		
		existingPost.setBody(updatedPostDto.getBody());
		if (updatedPostDto.getImageId() != null) {
			Image image = new Image();
			image.setId(updatedPostDto.getImageId());
			existingPost.setImage(image);
		}
		existingPost.setUpdatedAt(new Date());
		var updatedPost = postsCrudRepository.save(existingPost);
		return PostMapper.MAPPER.postToPostUpdateResponseDto(updatedPost);
	}
	
	public void delete(UUID id, UUID userId) {
		var foundPost = postsCrudRepository.findByIdAndUserId(id, userId)
				.orElseThrow();
		
		foundPost.setIsDeleted(true);
		postsCrudRepository.save(foundPost);
	}
}
