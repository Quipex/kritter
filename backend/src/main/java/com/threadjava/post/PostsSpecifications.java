package com.threadjava.post;

import com.threadjava.post.model.Post;
import com.threadjava.reactions.posts.model.PostReaction;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.UUID;

public class PostsSpecifications {
	
	public static Specification<Post> isAuthorsPost(UUID authorId) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("user").get("id"), authorId);
	}
	
	public static Specification<Post> notAuthorsPost(UUID authorId) {
		return ((root, query, criteriaBuilder) -> criteriaBuilder.notEqual(root.get("user").get("id"), authorId));
	}
	
	public static Specification<Post> wasLikedBy(UUID userId) {
		return ((root, query, criteriaBuilder) -> {
			var prSubQuery = query.subquery(PostReaction.class);
			Root<PostReaction> pr = prSubQuery.from(PostReaction.class);
			Predicate isLiked = criteriaBuilder.equal(pr.get("isLike"), true);
			Predicate ourUser = criteriaBuilder.equal(pr.get("user").get("id"), userId);
			return root.get("id").in(prSubQuery.select(pr.get("post").get("id")).where(isLiked, ourUser));
		});
	}
}
