package com.threadjava.post;

import lombok.Data;

import java.util.UUID;

@Data
public class QueryFilter {
	private AuthorsPostsFilter authorsFilter;
	private boolean showOnlyLiked;
	private UUID authorId;
	
	public enum AuthorsPostsFilter {
		/**
		 * Show only author's posts
		 */
		SHOW_ONLY,
		
		/**
		 * Hides author's posts
		 */
		HIDE
	}
}
