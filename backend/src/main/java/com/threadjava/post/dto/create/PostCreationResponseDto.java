package com.threadjava.post.dto.create;

import lombok.Data;

import java.util.UUID;

@Data
public class PostCreationResponseDto {
    private UUID id;
    private UUID userId;
}
