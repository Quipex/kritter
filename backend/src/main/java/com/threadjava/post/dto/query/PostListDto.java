package com.threadjava.post.dto.query;

import com.threadjava.image.dto.ImageDto;
import com.threadjava.post.dto.PostUserDto;
import lombok.Data;
import java.util.Date;
import java.util.UUID;

@Data
public class PostListDto {
    private UUID id;
    private String body;
    private long likeCount;
    private long dislikeCount;
    private long commentCount;
    private Date createdAt;
    private Date updatedAt;
    private ImageDto image;
    private PostUserDto user;
}
