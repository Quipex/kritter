package com.threadjava.post.dto.query;


import com.threadjava.image.model.Image;
import com.threadjava.users.model.User;
import lombok.*;
import java.util.Date;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostListQueryResult {
    private UUID id;
    private String body;
    private long likeCount;
    private long dislikeCount;
    private long commentCount;
    private Date createdAt;
    private Date updatedAt;
    private Image image;
    private User user;
}
