package com.threadjava.post.dto.query;

import com.threadjava.post.QueryFilter;
import lombok.Data;

@Data
public class PostsRequestDto {
	private Integer from = 0;
	private Integer count = 10;
	private QueryFilter filter;
}
