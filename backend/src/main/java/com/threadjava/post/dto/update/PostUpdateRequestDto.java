package com.threadjava.post.dto.update;

import lombok.Data;

import java.util.UUID;

@Data
public class PostUpdateRequestDto {
	private UUID id;
	private String body;
	private UUID imageId;
}
