package com.threadjava.post.dto.update;

import lombok.Data;

import java.util.UUID;

@Data
public class PostUpdateResponseDto {
	private UUID id;
	private UUID userId;
}
