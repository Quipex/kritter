package com.threadjava.post.model;

import com.threadjava.comment.model.Comment;
import com.threadjava.db.BaseEntity;
import com.threadjava.image.model.Image;
import com.threadjava.reactions.common.Reaction;
import com.threadjava.reactions.posts.model.PostReaction;
import com.threadjava.users.model.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Table(name = "posts")
@Where(clause = "is_deleted IS false OR is_deleted IS NULL")
public class Post extends BaseEntity {
	
	@Column(name = "body", columnDefinition = "TEXT")
	private String body;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "image_id")
	private Image image;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "user_id")
	private User user;
	
	@OneToMany(mappedBy = "post", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Comment> comments = new ArrayList<>();
	
	@OneToMany(mappedBy = "post", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<PostReaction> reactions = new ArrayList<>();
	
	@Column(name = "is_deleted")
	private Boolean isDeleted;
	
	@Override
	public String toString() {
		long likes = reactions.stream().filter(Reaction::getIsLike).count();
		long dislikes = reactions.stream().filter(Predicate.not(Reaction::getIsLike)).count();
		return "{\"_class\":\"Post\", " +
				"\"body\":" + (body == null ? "null" : "\"" + body + "\"") + ", " +
				"\"image\":" + (image == null ? "null" : image) + ", " +
				"\"user\":" + (user == null ? "null" : user) + ", " +
				"\"comments\":" + (comments == null ? "null" : comments.size()) + ", " +
				"\"likes\":" + likes + ", " +
				"\"dislikes\":" + dislikes + ", " +
				"\"isDeleted\":" + (isDeleted == null ? "null" : "\"" + isDeleted + "\"") +
				"}";
	}
}
