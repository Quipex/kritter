package com.threadjava.reactions.comments;

import com.threadjava.reactions.common.ResponseReactionDto;
import com.threadjava.reactions.comments.dto.CommentReactionDto;
import com.threadjava.reactions.comments.dto.ReceivedCommentReactionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/comment_reaction")
public class CommentReactionController {
	
	@Autowired
	private CommentReactionService commentReactionService;
	
	@PutMapping
	public ResponseReactionDto setReaction(@RequestBody ReceivedCommentReactionDto receivedReaction) {
		receivedReaction.setUserId(getUserId());
		return commentReactionService.setReactionAndGetCommentRating(receivedReaction);
	}
	
	@PostMapping
	public List<CommentReactionDto> getUserReactionsFor(@RequestBody Iterable<UUID> postIds) {
		return commentReactionService.getUserReactionsFor(getUserId(), postIds);
	}
}
