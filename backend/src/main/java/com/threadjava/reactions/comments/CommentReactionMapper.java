package com.threadjava.reactions.comments;

import com.threadjava.reactions.comments.dto.CommentReactionDto;
import com.threadjava.reactions.comments.dto.ReceivedCommentReactionDto;
import com.threadjava.reactions.comments.model.CommentReaction;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CommentReactionMapper {
	CommentReactionMapper MAPPER = Mappers.getMapper( CommentReactionMapper.class );
	
	@Mapping(source = "comment.id", target = "id")
	CommentReactionDto reactionToDtoReaction(CommentReaction postReaction);
	
	@Mapping(source = "userId", target = "user.id")
	@Mapping(source = "commentId", target = "comment.id")
	@Mapping(target = "id", ignore = true)
	@Mapping(target = "createdAt", ignore = true)
	@Mapping(target = "updatedAt", ignore = true)
	CommentReaction dtoToCommentReaction(ReceivedCommentReactionDto postReactionDto);
}
