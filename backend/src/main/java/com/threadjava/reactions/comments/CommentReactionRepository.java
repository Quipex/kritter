package com.threadjava.reactions.comments;

import com.threadjava.reactions.comments.model.CommentReaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface CommentReactionRepository extends JpaRepository<CommentReaction, UUID> {
	
	List<CommentReaction> getAllByCommentId(UUID commentId);
	
	@Query("SELECT r " +
			"FROM CommentReaction r " +
			"WHERE r.user.id = :userId and r.comment.id IN :commentIds ")
	List<CommentReaction> getBatchReactionsOfUser(UUID userId, Iterable<UUID> commentIds);
	
	@Modifying
	@Query("delete from CommentReaction cr where cr.id=:id ")
//	hack because hibernate does not remove entity
	void deleteById(UUID id);
}
