package com.threadjava.reactions.comments;

import com.threadjava.reactions.comments.dto.CommentReactionDto;
import com.threadjava.reactions.comments.dto.ReceivedCommentReactionDto;
import com.threadjava.reactions.comments.model.CommentReaction;
import com.threadjava.reactions.common.ReactionCalculator;
import com.threadjava.reactions.common.ReactionService;
import com.threadjava.reactions.common.ReactionsMapper;
import com.threadjava.reactions.common.ResponseReactionDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Slf4j
public class CommentReactionService {
	private final CommentReactionRepository commentReactionRepository;
	private final ReactionService<CommentReaction> reactionService;
	
	public CommentReactionService(CommentReactionRepository commentReactionRepository) {
		this.commentReactionRepository = commentReactionRepository;
		this.reactionService = new ReactionService<>(commentReactionRepository);
	}
	
	public ResponseReactionDto setReactionAndGetCommentRating(ReceivedCommentReactionDto receivedReaction) {
		log.debug("Received comment reaction for applying: {}", receivedReaction);
		CommentReaction newReaction = CommentReactionMapper.MAPPER.dtoToCommentReaction(receivedReaction);
		List<CommentReaction> oldReactions = commentReactionRepository.getAllByCommentId(receivedReaction.getCommentId());
		var rating = ReactionCalculator.calculateRating(newReaction, oldReactions);
		var resultingReaction = reactionService.applyReaction(newReaction, oldReactions).orElse(new CommentReaction());
		return ReactionsMapper.MAPPER.reactionToResponseReactionDto(resultingReaction, rating.getLikeCount(), rating.getDislikeCount());
	}
	
	public List<CommentReactionDto> getUserReactionsFor(UUID userId, Iterable<UUID> commentIds) {
		if (userId == null) {
			return new ArrayList<>();
		}
		var reactionsOfUser = commentReactionRepository.getBatchReactionsOfUser(userId, commentIds);
		return reactionsOfUser.stream().map(CommentReactionMapper.MAPPER::reactionToDtoReaction).collect(Collectors.toList());
	}
}
