package com.threadjava.reactions.comments.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommentReactionDto {
	private UUID id;
	private Boolean isLike;
}
