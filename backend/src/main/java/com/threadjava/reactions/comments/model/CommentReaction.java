package com.threadjava.reactions.comments.model;

import com.threadjava.comment.model.Comment;
import com.threadjava.db.BaseEntity;
import com.threadjava.reactions.common.Reaction;
import com.threadjava.users.model.User;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Data
@EqualsAndHashCode(callSuper=true)
@Table(name = "comment_reactions")
public class CommentReaction extends BaseEntity implements Reaction {
	
	@Column(name = "is_like")
	private Boolean isLike;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "user_id")
	private User user;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "comment_id")
	private Comment comment;
	
	@Override
	public UUID getReactableId() {
		return comment != null ? comment.getId() : null;
	}
	
	@Override
	public Boolean getIsLike() {
		return isLike;
	}
	
	@Override
	public UUID getUserId() {
		return user != null ? user.getId() : null;
	}
	
	@Override
	public String toString() {
		return "{\"_super\": " + super.toString() + ", " +
				"\"_class\":\"CommentReaction\", " +
				"\"isLike\":" + (isLike == null ? "null" : "\"" + isLike + "\"") + ", " +
				"\"user\":" + (user == null ? "null" : user) +
				"\"comment\":" + (comment == null ? "null" : comment) + ", " +
				"}";
	}
}
