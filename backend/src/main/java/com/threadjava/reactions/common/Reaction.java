package com.threadjava.reactions.common;

import java.util.UUID;

public interface Reaction {
	UUID getId();
	
	@SuppressWarnings("unused")
//	used by com\threadjava\reactions\common\ReactionsMapper.java
	UUID getReactableId();
	
	Boolean getIsLike();
	
	UUID getUserId();
}
