package com.threadjava.reactions.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.function.Predicate;

@Slf4j
public class ReactionCalculator {
	
	public static RatingCount calculateRating(Reaction newUserReaction, List<? extends Reaction> oldReactions) {
		log.debug("Calculating new rating: new reaction: {}", newUserReaction);
		var foundOldUserReaction = oldReactions.stream().filter(r -> r.getUserId().equals(newUserReaction.getUserId())).findFirst();
		long likes = oldReactions.stream().filter(Reaction::getIsLike).count();
		long dislikes = oldReactions.stream().filter(Predicate.not(Reaction::getIsLike)).count();
		log.debug("Old rating: likes={} dislikes={}", likes, dislikes);
		if (foundOldUserReaction.isPresent()) {
			var oldUserReaction = foundOldUserReaction.get();
			log.debug("Old user reaction exists: {}", oldUserReaction);
			if (oldUserReaction.getIsLike() == newUserReaction.getIsLike()) {
				return calculateWhenRemovedReaction(likes, dislikes, oldUserReaction.getIsLike());
			} else {
				return calculateWhenOppositeReaction(likes, dislikes, oldUserReaction.getIsLike());
			}
		} else {
			log.debug("Old user reaction does not exist.");
			return calculateWhenNoReaction(likes, dislikes, newUserReaction.getIsLike());
		}
	}
	
	@Data
	@AllArgsConstructor
	public static class RatingCount {
		private long likeCount;
		private long dislikeCount;
	}
	
	private static RatingCount calculateWhenRemovedReaction(long likes, long dislikes, boolean oldWasLike) {
		if (oldWasLike) {
			likes--;
		} else {
			dislikes--;
		}
		log.debug("Reaction was removed: likes={}, dislikes={}", likes, dislikes);
		return new RatingCount(likes, dislikes);
	}
	
	private static RatingCount calculateWhenOppositeReaction(long likes, long dislikes, boolean oldWasLike) {
		if (oldWasLike) {
			likes--;
			dislikes++;
		} else {
			dislikes--;
			likes++;
		}
		log.debug("User applied opposite reaction: likes={}, dislikes={}, reaction={}", likes, dislikes, oldWasLike ? "dislike" : "like");
		return new RatingCount(likes, dislikes);
	}
	
	private static RatingCount calculateWhenNoReaction(long likes, long dislikes, boolean newIsLike) {
		if (newIsLike) {
			likes++;
		} else {
			dislikes++;
		}
		log.debug("No reaction was before, applying new: likes={}, dislikes={}, reaction={}", likes, dislikes, newIsLike ? "like" : "dislike");
		return new RatingCount(likes, dislikes);
	}
}
