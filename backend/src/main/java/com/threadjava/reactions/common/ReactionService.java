package com.threadjava.reactions.common;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
public class ReactionService<T extends Reaction> {
	private final JpaRepository<T, UUID> reactionsRepository;
	
	public ReactionService(JpaRepository<T, UUID> reactionsRepository) {
		this.reactionsRepository = reactionsRepository;
	}
	
	public Optional<T> applyReaction(T newReaction, List<T> oldReactions) {
		log.debug("Applying user reaction: reaction={}", newReaction);
		Optional<T> foundOldReaction = oldReactions.stream().filter(r -> r.getUserId().equals(newReaction.getUserId())).findFirst();
		if (foundOldReaction.isPresent()) {
			var oldReaction = foundOldReaction.get();
			log.debug("deleting... {}", oldReaction.getId());
			reactionsRepository.deleteById(oldReaction.getId());
			log.debug("There was user reaction, deleted it: {}", oldReaction);
			if (oldReaction.getIsLike() != newReaction.getIsLike()) {
				log.debug("Saved opposite reaction: reaction={}", newReaction.getIsLike() ? "like" : "dislike");
				return Optional.of(reactionsRepository.save(newReaction));
			} else {
				log.debug("New reaction is the same, meaning user removed it. Doing nothing.");
				return Optional.empty();
			}
		} else {
			log.debug("No reaction before, saving new user reaction: {}", newReaction);
			return Optional.of(reactionsRepository.save(newReaction));
		}
	}
}
