package com.threadjava.reactions.common;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ReactionsMapper {
	
	ReactionsMapper MAPPER = Mappers.getMapper(ReactionsMapper.class);
	
	@Mapping(source = "reaction.isLike", target = "isLike")
	@Mapping(source = "reaction.id", target = "id")
	@Mapping(source = "reaction.reactableId", target = "reactableId")
	@Mapping(source = "reaction.userId", target = "userId")
	ResponseReactionDto reactionToResponseReactionDto(Reaction reaction, long likeCount, long dislikeCount);
}
