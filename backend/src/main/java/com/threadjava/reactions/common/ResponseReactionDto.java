package com.threadjava.reactions.common;

import lombok.Data;

import java.util.UUID;

@Data
public class ResponseReactionDto {
	private UUID id;
	private UUID reactableId;
	private Boolean isLike;
	private UUID userId;
	private long likeCount;
	private long dislikeCount;
}
