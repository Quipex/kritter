package com.threadjava.reactions.posts;

import com.threadjava.reactions.common.ResponseReactionDto;
import com.threadjava.reactions.posts.dto.PostReactionDto;
import com.threadjava.reactions.posts.dto.ReceivedPostReactionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/post_reaction")
public class PostReactionController {
	@Autowired
	private PostReactionService postReactionService;
	@Autowired
	private SimpMessagingTemplate template;
	
	@PutMapping
	public ResponseReactionDto setReaction(@RequestBody ReceivedPostReactionDto postReaction) {
		postReaction.setUserId(getUserId());
		var reaction = postReactionService.setReactionAndGetUpdatedPostRating(postReaction);
		
		if (reaction != null && reaction.getIsLike() != null && reaction.getIsLike()
				&& reaction.getUserId() != getUserId()) {
//			fixme add check that post author is you
			// notify a user if someone (not himself) liked his post
			
			template.convertAndSend("/topic/like", "Your post was liked!");
		}
		return reaction;
	}
	
	@PostMapping
	public List<PostReactionDto> getUserReactionsFor(@RequestBody Iterable<UUID> postIds) {
		return postReactionService.getUserReactionsFor(getUserId(), postIds);
	}
}
