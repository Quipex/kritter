package com.threadjava.reactions.posts;

import com.threadjava.reactions.posts.dto.PostReactionDto;
import com.threadjava.reactions.posts.dto.ReceivedPostReactionDto;
import com.threadjava.reactions.posts.model.PostReaction;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PostReactionMapper {
    PostReactionMapper MAPPER = Mappers.getMapper( PostReactionMapper.class );

    @Mapping(source = "post.id", target = "id")
    PostReactionDto reactionToDtoReaction(PostReaction postReaction);

    @Mapping(source = "userId", target = "user.id")
    @Mapping(source = "postId", target = "post.id")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    PostReaction dtoToPostReaction(ReceivedPostReactionDto postReactionDto);
}
