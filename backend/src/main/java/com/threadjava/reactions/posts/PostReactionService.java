package com.threadjava.reactions.posts;

import com.threadjava.reactions.common.*;
import com.threadjava.reactions.posts.dto.PostReactionDto;
import com.threadjava.reactions.posts.dto.ReceivedPostReactionDto;
import com.threadjava.reactions.posts.model.PostReaction;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Slf4j
public class PostReactionService {
	private final PostReactionsRepository postReactionsRepository;
	private final ReactionService<PostReaction> reactionService;
	
	public PostReactionService(PostReactionsRepository postReactionsRepository) {
		this.postReactionsRepository = postReactionsRepository;
		this.reactionService = new ReactionService<>(postReactionsRepository);
	}
	
	public ResponseReactionDto setReactionAndGetUpdatedPostRating(ReceivedPostReactionDto receivedReaction) {
		log.debug("Received post reaction for applying: {}", receivedReaction);
		PostReaction newReaction = PostReactionMapper.MAPPER.dtoToPostReaction(receivedReaction);
		List<PostReaction> oldReactions = postReactionsRepository.getAllByPostId(receivedReaction.getPostId());
		var rating = ReactionCalculator.calculateRating(newReaction, oldReactions);
		var resultingReaction = reactionService.applyReaction(newReaction, oldReactions).orElse(new PostReaction());
		return ReactionsMapper.MAPPER.reactionToResponseReactionDto(resultingReaction, rating.getLikeCount(), rating.getDislikeCount());
	}
	
	public List<PostReactionDto> getUserReactionsFor(UUID userId, Iterable<UUID> postIds) {
		if (userId == null) {
			return new ArrayList<>();
		}
		var reactionsOfUser = postReactionsRepository.getBatchReactionsOfUser(userId, postIds);
		return reactionsOfUser.stream().map(PostReactionMapper.MAPPER::reactionToDtoReaction).collect(Collectors.toList());
	}
}
