package com.threadjava.reactions.posts;

import com.threadjava.reactions.posts.model.PostReaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PostReactionsRepository extends JpaRepository<PostReaction, UUID> {
    
    List<PostReaction> getAllByPostId(UUID postId);
    
    @Query("SELECT r " +
			"FROM PostReaction r " +
			"WHERE r.user.id = :userId and r.post.id IN :postIds ")
	List<PostReaction> getBatchReactionsOfUser(UUID userId, Iterable<UUID> postIds);
    
    @Modifying
    @Query("delete from PostReaction pr where pr.id=:id ")
//	hack because hibernate does not remove entity
    void deleteById(UUID id);
}
