package com.threadjava.reactions.posts.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostReactionDto {
    private UUID id;
    private Boolean isLike;
}
