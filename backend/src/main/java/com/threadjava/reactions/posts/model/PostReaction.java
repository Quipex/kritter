package com.threadjava.reactions.posts.model;

import com.threadjava.db.BaseEntity;
import com.threadjava.post.model.Post;
import com.threadjava.reactions.common.Reaction;
import com.threadjava.users.model.User;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Table(name = "post_reactions")
public class PostReaction extends BaseEntity implements Reaction {
	
	@Column(name = "is_like")
	private Boolean isLike;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "user_id")
	private User user;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "post_id")
	private Post post;
	
	@Override
	public UUID getReactableId() {
		return post != null ? post.getId() : null;
	}

	@Override
	public Boolean getIsLike() {
		return isLike;
	}

	@Override
	public UUID getUserId() {
		return user != null ? user.getId() : null;
	}
	
	@Override
	public String toString() {
		return "{\"_super\": " + super.toString() + ", " +
				"\"_class\":\"PostReaction\", " +
				"\"isLike\":" + (isLike == null ? "null" : "\"" + isLike + "\"") + ", " +
				"\"user\":" + (user == null ? "null" : user) +
				"\"post\":" + (post == null ? "null" : post) + ", " +
				"}";
	}
}
