package com.threadjava.users;

import com.threadjava.users.dto.StatusDto;
import com.threadjava.users.dto.UserDetailsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    private UsersService userDetailsService;

    @GetMapping
    public UserDetailsDto getUser() {
        return userDetailsService.getUserById(getUserId());
    }
    
    @PutMapping("status")
    public StatusDto updateStatus(@RequestBody StatusDto status) {
        return userDetailsService.updateStatus(status, getUserId());
    }
}
