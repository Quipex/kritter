package com.threadjava.users.model;

import com.threadjava.db.BaseEntity;
import com.threadjava.image.model.Image;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity
@Data
@EqualsAndHashCode(callSuper=true)
@Table(name = "users")
public class User extends BaseEntity {
    @Column(name = "email", unique=true)
    private String email;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "avatar_id")
    private Image avatar;
    
    @Column(name = "status")
    private String status;
    
    @Override
    public String toString() {
        return "{\"_class\":\"User\", " +
                "\"email\":" + (email == null ? "null" : "\"" + email + "\"") + ", " +
                "\"username\":" + (username == null ? "null" : "\"" + username + "\"") + ", " +
                "\"password\":" + (password == null ? "null" : "\"" + password + "\"") + ", " +
                "\"avatar\":" + (avatar == null ? "null" : avatar) +
                "}";
    }
}
