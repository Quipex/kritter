package com.threadjava.email;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MailServiceTest {
	@Autowired
	private MailService mailService;
	
	@Test
	void sendTestMail() {
		mailService.generateAndSendMail("This is a test mail", "Just a test", "rudas72@gmail.com");
	}
}
