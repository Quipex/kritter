import React from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Header, Icon, Label } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';

const Comment = ({ comment, rateComment, currentUser, toggleEditedComment, deleteComment }) => {
  const {
    id,
    body,
    createdAt,
    updatedAt,
    user,
    likeCount,
    dislikeCount,
    isLiked,
    isDisliked
  } = comment;

  const createdDate = moment(createdAt).fromNow();
  const dateUpdated = moment(updatedAt).fromNow();

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={getUserImgLink(user.image)} />
      <CommentUI.Content>
        <Header className={styles.nick_status}>
          <Header.Content as="h4">
            <CommentUI.Author as="a">
              {user.username}
            </CommentUI.Author>
            <CommentUI.Metadata>
              {createdDate}
              {createdAt !== updatedAt && `. Updated ${dateUpdated}`}
            </CommentUI.Metadata>
            <Header.Subheader>
              {user.status && (
                <CommentUI.Metadata content={user.status} className={styles.status} />)}
            </Header.Subheader>
          </Header.Content>
        </Header>
        <CommentUI.Text>
          {body}
        </CommentUI.Text>
        <CommentUI.Actions>
          <CommentUI.Action>
            <Label
              basic
              size="small"
              className={`${styles.toolbarBtn} ${isLiked ? styles.liked : ''}`}
              onClick={() => rateComment(comment, true)}
            >
              <Icon name="thumbs up" />
              {likeCount}
            </Label>
          </CommentUI.Action>
          <CommentUI.Action>
            <Label
              basic
              size="small"
              className={`${styles.toolbarBtn} ${isDisliked ? styles.disliked : ''}`}
              onClick={() => rateComment(comment, false)}
            >
              <Icon name="thumbs down" />
              {dislikeCount}
            </Label>
          </CommentUI.Action>
          {user.id === currentUser
          && (
            <>
              <CommentUI.Action>
                <Label
                  basic
                  size="small"
                  className={styles.toolbarBtn}
                  onClick={() => toggleEditedComment(comment)}
                >
                  <Icon name="edit" />
                </Label>
              </CommentUI.Action>
              <CommentUI.Action>
                <Label
                  basic
                  size="small"
                  className={styles.toolbarBtn}
                  onClick={() => deleteComment(id)}
                >
                  <Icon name="trash alternate" />
                </Label>
              </CommentUI.Action>
            </>
          )}
        </CommentUI.Actions>
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  rateComment: PropTypes.func.isRequired,
  currentUser: PropTypes.string.isRequired,
  toggleEditedComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired
};

export default Comment;
