import { Button, Form, Label, Modal } from 'semantic-ui-react';
import React, { useState } from 'react';
import Spinner from 'src/components/Spinner';
import PropTypes from 'prop-types';
import styles from './styles.module.scss';

const EditStatus = ({ status, updateStatus, toggleEditedStatus: toggle }) => {
  const [statusValue, setStatusValue] = useState(status === undefined ? '' : status);
  const [errorMessage, setErrorMessage] = useState('');
  const [updating, setUpdating] = useState(false);

  const handleUpdateStatus = async () => {
    try {
      setUpdating(true);
      await updateStatus(statusValue);
      toggle();
    } catch (e) {
      setErrorMessage(e.message === undefined ? 'Error occured' : e.message);
    } finally {
      setUpdating(false);
    }
  };

  return (
    <Modal dimmer="blurring" size="tiny" centered open onClose={() => toggle()}>
      {status !== undefined
        ? (
          <>
            <Modal.Header>Edit status</Modal.Header>
            <Modal.Content>
              {errorMessage !== '' && <Label basic color="red" className={styles.error_label}>{errorMessage}</Label>}
              <Form onSubmit={handleUpdateStatus}>
                <Form.Input
                  label="New status"
                  type="text"
                  fluid
                  onChange={event => setStatusValue(event.target.value)}
                  value={statusValue}
                />
                <Button
                  type="submit"
                  loading={updating}
                  primary
                  floated="right"
                  className={styles.submit}
                  labelPosition="left"
                  content="Update"
                  icon="edit"
                />
              </Form>
            </Modal.Content>
          </>
        )
        : <Spinner />}
    </Modal>
  );
};

EditStatus.propTypes = {
  status: PropTypes.string,
  updateStatus: PropTypes.func.isRequired,
  toggleEditedStatus: PropTypes.func.isRequired
};

EditStatus.defaultProps = {
  status: undefined
};

export default EditStatus;
