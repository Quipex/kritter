import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { getUserImgLink } from 'src/helpers/imageHelper';
import { Button, Header as HeaderUI, Icon, Image, Menu } from 'semantic-ui-react';

import styles from './styles.module.scss';

const Header = ({ user, logout, toggleEditedStatus: toggle }) => (
  <Menu size="small">
    <Menu.Item header className={styles.noPadding}>
      <NavLink exact to="/" className={`${styles.navButton} ${styles.menuBtn}`}>
        Home
      </NavLink>
    </Menu.Item>
    {user && (
      <Menu.Item header>
        <HeaderUI>
          <Image circular src={getUserImgLink(user.image)} />
          <HeaderUI.Content>
            {' '}
            {user.username}
            <HeaderUI.Subheader>
              <span onClick={() => toggle(user.status)} className={styles.status}>{user.status}</span>
            </HeaderUI.Subheader>
          </HeaderUI.Content>
        </HeaderUI>
      </Menu.Item>
    )}
    <Menu.Menu position="right">
      <Menu.Item>
        <NavLink exact activeClassName="active" to="/profile" className={styles.menuBtn}>
          <Icon name="user circle" size="large" />
        </NavLink>
      </Menu.Item>
      <Menu.Item>
        <Button basic icon type="button" className={`${styles.menuBtn} ${styles.logoutBtn}`} onClick={logout}>
          <Icon name="log out" size="large" />
        </Button>
      </Menu.Item>
    </Menu.Menu>
  </Menu>
);

Header.propTypes = {
  logout: PropTypes.func.isRequired,
  toggleEditedStatus: PropTypes.func.isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired
};

export default Header;
