import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Button, Form, Label, Modal, Segment } from 'semantic-ui-react';
import styles from '../ResetPasswordForm/styles.module.scss';

const NewPasswordForm = ({ changePassword, email, token }) => {
  const [isLoading, setLoading] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const [successMessage, setSuccessMessage] = useState(undefined);
  const [isPassword1Valid, setPassword1Valid] = useState(true);
  const [password1, setPassword1] = useState('');

  const handleSetPasswordClick = async () => {
    if (!isPassword1Valid || isLoading) {
      return;
    }
    setLoading(true);
    try {
      await changePassword({
        email,
        token,
        password: password1
      });
      setSuccessMessage('Password has been changed successfully!');
    } catch (e) {
      const msg = e.errors[0] !== undefined ? e.errors[0] : e.message;
      setErrorMessage(msg !== undefined ? msg : 'Unknown error');
    } finally {
      setLoading(false);
    }
  };

  const password1Changed = value => {
    setPassword1(value);
    setPassword1Valid(true);
  };

  return (
    <>
      <Form name="newPasswordForm" size="large" onSubmit={handleSetPasswordClick}>
        <Segment>
          {errorMessage !== '' && <Label basic color="red" className={styles.error_label}>{errorMessage}</Label>}
          <Form.Input
            label="Your email"
            fluid
            readOnly
            icon="at"
            iconPosition="left"
            placeholder="Email"
            type="email"
            value={email}
          />
          <Form.Input
            label="Enter your new password"
            fluid
            icon="lock"
            iconPosition="left"
            placeholder="Password"
            type="password"
            onChange={ev => password1Changed(ev.target.value)}
            error={!isPassword1Valid}
            onBlur={() => setPassword1Valid(Boolean(password1))}
          />
          <Button type="submit" color="teal" fluid size="large" loading={isLoading} primary>
            Submit
          </Button>
        </Segment>
      </Form>
      {successMessage && (
        <Modal size="mini" open onClose={() => setSuccessMessage(undefined)}>
          <Modal.Header className={styles.modal_header_success}>Success</Modal.Header>
          <Modal.Content>
            {successMessage}
          </Modal.Content>
        </Modal>
      )}
    </>
  );
};

NewPasswordForm.propTypes = {
  changePassword: PropTypes.func.isRequired,
  email: PropTypes.string.isRequired,
  token: PropTypes.string.isRequired
};

export default NewPasswordForm;
