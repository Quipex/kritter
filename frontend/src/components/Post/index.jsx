import React from 'react';
import PropTypes from 'prop-types';
import { Card, Dropdown, Icon, Image, Label } from 'semantic-ui-react';
import moment from 'moment';

import styles from './styles.module.scss';

const Post = ({ post, ratePost, toggleExpandedPost, sharePost, deletePost, currentUser, toggleEditedPost }) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt,
    updatedAt,
    isLiked,
    isDisliked
  } = post;
  const date = moment(createdAt)
    .fromNow();
  const dateUpdated = moment(updatedAt).fromNow();

  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            <Label horizontal color={user.id === currentUser ? 'teal' : undefined}>
              {user.username}
            </Label>
            {' - '}
            {date}
            {createdAt !== updatedAt && `. Updated ${dateUpdated}`}
          </span>
          {user.id === currentUser
          && (
            <Dropdown icon="ellipsis vertical" compact className={styles.settings}>
              <Dropdown.Menu>
                <Dropdown.Item text="Edit" onClick={() => toggleEditedPost(id)} />
                <Dropdown.Item text="Delete" onClick={() => deletePost(id)} />
              </Dropdown.Menu>
            </Dropdown>
          )}
        </Card.Meta>
        <Card.Description>
          {body}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Label
          basic
          size="small"
          as="a"
          className={`${styles.toolbarBtn} ${isLiked ? styles.liked : ''}`}
          onClick={() => ratePost(post, true)}
        >
          <Icon name="thumbs up" />
          {likeCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={`${styles.toolbarBtn} ${isDisliked ? styles.disliked : ''}`}
          onClick={() => ratePost(post, false)}
        >
          <Icon name="thumbs down" />
          {dislikeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
          <Icon name="comment" />
          {commentCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
          <Icon name="share alternate" />
        </Label>
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  ratePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  toggleEditedPost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  currentUser: PropTypes.string
};

Post.defaultProps = {
  currentUser: undefined
};

export default Post;
