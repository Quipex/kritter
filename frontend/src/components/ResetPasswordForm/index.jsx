import React, { useState } from 'react';
import { Button, Form, Label, Modal, Segment } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import validator from 'validator';
import styles from './styles.module.scss';

const ResetPasswordForm = ({ restore }) => {
  const [email, setEmail] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isEmailValid, setIsEmailValid] = useState(true);
  const [errorMessage, setErrorMessage] = useState('');
  const [successMessage, setSuccessMessage] = useState(undefined);

  const emailChanged = data => {
    setEmail(data);
    setIsEmailValid(true);
  };

  const handleRestorePasswordClick = async () => {
    setIsLoading(true);
    try {
      await restore(email);
      setSuccessMessage('Restoration link was sent to your email');
    } catch (e) {
      setErrorMessage(e.status === 'NOT_FOUND' ? 'Account with given email does not exist' : 'Unknown error');
    }
    setIsLoading(false);
  };

  return (
    <>
      <Form name="passwordRestorationForm" size="large" onSubmit={handleRestorePasswordClick}>
        <Segment>
          {errorMessage !== '' && <Label basic color="red" className={styles.error_label}>{errorMessage}</Label>}
          <Form.Input
            label="Enter your email"
            fluid
            icon="at"
            iconPosition="left"
            placeholder="Email"
            type="email"
            error={!isEmailValid}
            onChange={ev => emailChanged(ev.target.value)}
            onBlur={() => setIsEmailValid(validator.isEmail(email))}
          />
          <Button type="submit" color="teal" fluid size="large" loading={isLoading} primary>
            Restore
          </Button>
        </Segment>
      </Form>
      {successMessage && (
        <Modal size="mini" open onClose={() => setSuccessMessage(undefined)}>
          <Modal.Header className={styles.modal_header_success}>Success</Modal.Header>
          <Modal.Content>
            {successMessage}
          </Modal.Content>
        </Modal>
      )}
    </>
  );
};

ResetPasswordForm.propTypes = {
  restore: PropTypes.func.isRequired
};

export default ResetPasswordForm;
