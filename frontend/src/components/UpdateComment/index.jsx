import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button } from 'semantic-ui-react';

const UpdateComment = ({
  comment,
  postId,
  updateComment,
  submitLabel
}) => {
  const [body, setBody] = useState(comment === undefined ? '' : comment.body);
  const [isPosting, setIsPosting] = useState(false);

  const handleUpdateComment = async () => {
    if (!body) {
      return;
    }
    setIsPosting(true);
    await updateComment({
      postId,
      body,
      id: comment?.id
    });
    setIsPosting(false);
    if (comment === undefined) {
      setBody('');
    }
  };

  return (
    <Form reply onSubmit={handleUpdateComment}>
      <Form.TextArea
        value={body}
        placeholder="Type a comment..."
        onChange={ev => setBody(ev.target.value)}
      />
      <Button type="submit" content={submitLabel} labelPosition="left" icon="edit" primary loading={isPosting} />
    </Form>
  );
};

UpdateComment.propTypes = {
  updateComment: PropTypes.func.isRequired,
  postId: PropTypes.string.isRequired,
  comment: PropTypes.objectOf(PropTypes.any),
  submitLabel: PropTypes.string
};

UpdateComment.defaultProps = {
  submitLabel: 'Post comment',
  comment: undefined
};

export default UpdateComment;
