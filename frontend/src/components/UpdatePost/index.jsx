import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Button, Form, Icon, Image, Label, Segment } from 'semantic-ui-react';

import styles from './styles.module.scss';

const UpdatePost = ({
  post,
  updatePost,
  uploadImage,
  submitLabel
}) => {
  const [body, setBody] = useState(post === undefined ? '' : post.body);
  const [image, setImage] = useState(post === undefined || post.image == null ? undefined : {
    imageId: post.image.id,
    imageLink: post.image.link
  });
  const [isUploading, setIsUploading] = useState(false);
  const [isPosting, setIsPosting] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');

  const handleUpdatePost = async () => {
    if (!body) {
      return;
    }
    setIsPosting(true);
    setErrorMessage('');
    try {
      await updatePost({
        imageId: image?.imageId,
        body,
        id: post?.id
      });
    } catch (e) {
      setErrorMessage(e.message);
    } finally {
      setIsPosting(false);
    }
    if (post === undefined) {
      setBody('');
      setImage(undefined);
    }
  };

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    setErrorMessage('');
    try {
      const { id: imageId, link: imageLink } = await uploadImage(target.files[0]);
      setImage({
        imageId,
        imageLink
      });
    } catch (error) {
      setErrorMessage(error.message);
    } finally {
      setIsUploading(false);
    }
  };

  return (
    <Segment>
      {errorMessage !== '' && <Label basic color="red" className={styles.error_label}>{errorMessage}</Label>}
      <Form onSubmit={handleUpdatePost}>
        <Form.TextArea
          name="body"
          value={body}
          placeholder="What is the news?"
          onChange={ev => setBody(ev.target.value)}
        />
        {image?.imageLink && (
          <div className={styles.imageWrapper}>
            <Image className={styles.image} src={image?.imageLink} alt="post" />
          </div>
        )}
        <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
          <Icon name="image" />
          Attach image
          <input name="image" type="file" onChange={handleUploadFile} hidden />
        </Button>
        <Button floated="right" color="blue" type="submit" loading={isPosting}>{submitLabel}</Button>
      </Form>
    </Segment>
  );
};

UpdatePost.defaultProps = {
  post: undefined,
  submitLabel: 'Post'
};

UpdatePost.propTypes = {
  updatePost: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  post: PropTypes.objectOf(PropTypes.any),
  submitLabel: PropTypes.string
};

export default UpdatePost;
