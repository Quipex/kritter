import { Label, Modal } from 'semantic-ui-react';
import Spinner from 'src/components/Spinner';
import styles from 'src/components/UpdatePost/styles.module.scss';
import UpdateComment from 'src/components/UpdateComment';
import PropTypes from 'prop-types';
import { toggleEditedComment, updateComment } from 'src/containers/Thread/actions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import React, { useState } from 'react';

const EditedComment = ({
  comment,
  toggleEditedComment: toggle,
  updateComment: updateC
}) => {
  const [errorMessage, setErrorMessage] = useState('');

  const handleUpdateClick = async _comment => {
    let resp;
    try {
      setErrorMessage('');
      resp = await updateC(_comment);
      toggle();
    } catch (e) {
      setErrorMessage(e.message);
    }
    return resp;
  };

  return (
    <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
      {comment
        ? (
          <>
            <Modal.Header>Edit a comment</Modal.Header>
            <Modal.Content>
              {errorMessage !== '' && <Label basic color="red" className={styles.error_label}>{errorMessage}</Label>}
              <UpdateComment
                updateComment={handleUpdateClick}
                submitLabel="Update comment"
                postId={comment.postId}
                comment={comment}
              />
            </Modal.Content>
          </>
        )
        : <Spinner />}
    </Modal>
  );
};

EditedComment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleEditedComment: PropTypes.func.isRequired,
  updateComment: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  comment: rootState.posts.editedComment
});

const actions = {
  toggleEditedComment,
  updateComment
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditedComment);
