import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Label, Modal } from 'semantic-ui-react';
import React, { useState } from 'react';
import Spinner from 'src/components/Spinner';
import UpdatePost from 'src/components/UpdatePost';
import { toggleEditedPost, updatePost } from 'src/containers/Thread/actions';
import styles from 'src/components/UpdatePost/styles.module.scss';
import * as imageService from '../../services/imageService';

const EditedPost = ({
  post,
  toggleEditedPost: toggle,
  updatePost: updateP
}) => {
  const [errorMessage, setErrorMessage] = useState('');

  const handleUpdateClick = async _post => {
    let resp;
    try {
      setErrorMessage('');
      resp = await updateP(_post);
      toggle();
    } catch (e) {
      setErrorMessage(e.message);
    }
    return resp;
  };

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
      {post
        ? (
          <>
            <Modal.Header>Edit a post</Modal.Header>
            <Modal.Content>
              {errorMessage !== '' && <Label basic color="red" className={styles.error_label}>{errorMessage}</Label>}
              <UpdatePost updatePost={handleUpdateClick} uploadImage={uploadImage} post={post} submitLabel="Update" />
            </Modal.Content>
          </>
        )
        : <Spinner />}
    </Modal>
  );
};

EditedPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleEditedPost: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  post: rootState.posts.editedPost
});

const actions = {
  toggleEditedPost,
  updatePost
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditedPost);
