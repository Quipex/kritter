import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Comment as CommentUI, Header, Modal } from 'semantic-ui-react';
import moment from 'moment';
import {
  addComment,
  deletePost,
  rateComment,
  ratePost,
  toggleEditedPost,
  toggleEditedComment,
  toggleExpandedPost,
  deleteComment
} from 'src/containers/Thread/actions';
import Post from 'src/components/Post';
import Comment from 'src/components/Comment';
import UpdateComment from 'src/components/UpdateComment';
import Spinner from 'src/components/Spinner';
import EditedComment from '../EditedComment';

const ExpandedPost = ({
  post,
  sharePost,
  ratePost: rateP,
  rateComment: rateC,
  toggleExpandedPost: toggleExpanded,
  addComment: addC,
  deleteComment: deleteC,
  userId,
  deletePost: deleteP,
  toggleEditedComment: toggleEditedC,
  toggleEditedPost: toggleEditedP,
  editedComment
}) => (
  <>
    <Modal dimmer="blurring" centered={false} open onClose={() => toggleExpanded()}>
      {post
        ? (
          <Modal.Content>
            <Post
              post={post}
              ratePost={rateP}
              toggleExpandedPost={toggleExpanded}
              sharePost={sharePost}
              currentUser={userId}
              deletePost={deleteP}
              toggleEditedPost={toggleEditedP}
            />
            <CommentUI.Group style={{ maxWidth: '100%' }}>
              <Header as="h3" dividing>
                Comments
              </Header>
              {post.comments && post.comments
                .sort((c1, c2) => moment(c1.createdAt)
                  .diff(c2.createdAt))
                .map(comment => (
                  <Comment
                    key={comment.id}
                    comment={comment}
                    currentUser={userId}
                    rateComment={rateC}
                    toggleEditedComment={toggleEditedC}
                    deleteComment={deleteC}
                  />
                ))}
              <UpdateComment postId={post.id} updateComment={addC} />
            </CommentUI.Group>
          </Modal.Content>
        )
        : <Spinner />}
    </Modal>
    {editedComment && <EditedComment />}
  </>
);

ExpandedPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  toggleEditedPost: PropTypes.func.isRequired,
  toggleEditedComment: PropTypes.func.isRequired,
  ratePost: PropTypes.func.isRequired,
  rateComment: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  userId: PropTypes.string,
  editedComment: PropTypes.objectOf(PropTypes.any),
  deleteComment: PropTypes.func.isRequired
};

ExpandedPost.defaultProps = {
  userId: undefined,
  editedComment: undefined
};

const mapStateToProps = rootState => ({
  post: rootState.posts.expandedPost,
  userId: rootState.profile.user.id,
  editedComment: rootState.posts.editedComment
});

const actions = {
  ratePost,
  toggleExpandedPost,
  addComment,
  deletePost,
  toggleEditedPost,
  toggleEditedComment,
  rateComment,
  deleteComment
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpandedPost);
