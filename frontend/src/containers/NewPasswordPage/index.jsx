import React from 'react';
import { Grid, Header, Message } from 'semantic-ui-react';
import { NavLink, useLocation } from 'react-router-dom';
import { setNewPassword } from 'src/containers/Profile/actions';
import Logo from 'src/components/Logo';
import NewPasswordForm from 'src/components/NewPasswordForm';

const NewPasswordPage = () => {
  const useQuery = () => new URLSearchParams(useLocation().search);

  return (
    <Grid textAlign="center" verticalAlign="middle" className="fill">
      <Grid.Column style={{ maxWidth: 450 }}>
        <Logo />
        <Header as="h2" color="teal" textAlign="center">
          Restore your password
        </Header>
        <NewPasswordForm
          email={useQuery().get('email')}
          token={useQuery().get('token')}
          changePassword={setNewPassword}
        />
        <Message>
          Back to the login page
          {' '}
          <NavLink exact to="/login">Log In</NavLink>
        </Message>
      </Grid.Column>
    </Grid>
  );
};

export default NewPasswordPage;
