import * as authService from 'src/services/authService';
import * as profileService from 'src/services/profileService';
import { SET_EDITED_STATUS, SET_USER, UPDATE_STATUS } from 'src/containers/Profile/actionTypes';

const setToken = token => localStorage.setItem('token', token);

const setUser = user => async dispatch => dispatch({
  type: SET_USER,
  user
});

const setEditedStatusAction = status => ({
  type: SET_EDITED_STATUS,
  status
});

const updateStatusAction = status => ({
  type: UPDATE_STATUS,
  status
});

const setAuthData = (user = null, token = '') => (dispatch, getRootState) => {
  setToken(token); // token should be set first before user
  setUser(user)(dispatch, getRootState);
};

const handleAuthResponse = authResponsePromise => async (dispatch, getRootState) => {
  const { user, token } = await authResponsePromise;
  setAuthData(user, token)(dispatch, getRootState);
};

export const login = request => handleAuthResponse(authService.login(request));

export const register = request => handleAuthResponse(authService.registration(request));

export const logout = () => setAuthData();

export const requestResetPassword = async email => {
  await authService.requestResetPassword({ email });
};

export const setNewPassword = async request => {
  await authService.setNewPassword(request);
};

export const loadCurrentUser = () => async (dispatch, getRootState) => {
  const user = await authService.getCurrentUser();
  setUser(user)(dispatch, getRootState);
};

export const toggleEditedStatus = status => async dispatch => {
  dispatch(setEditedStatusAction(status));
};

export const updateStatus = status => async dispatch => {
  const response = await profileService.updateStatus(status);
  dispatch(updateStatusAction(response.status));
};
