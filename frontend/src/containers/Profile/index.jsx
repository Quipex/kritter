import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getUserImgLink } from 'src/helpers/imageHelper';
import { Button, Grid, Image, Input } from 'semantic-ui-react';
import EditStatus from 'src/components/EditStatus';
import { bindActionCreators } from 'redux';
import { toggleEditedStatus, updateStatus } from 'src/containers/Profile/actions';
import styles from './styles.module.scss';

const Profile = ({ user, editedStatus, toggleEditedStatus: toggle, updateStatus: update }) => (
  <>
    <Grid container textAlign="center" style={{ paddingTop: 30 }}>
      <Grid.Column width={6}>
        <Image centered src={getUserImgLink(user.image)} size="medium" circular />
        <br />
        <Input
          icon="user"
          iconPosition="left"
          placeholder="Username"
          type="text"
          disabled
          value={user.username}
        />
        <br />
        <br />
        <Input
          icon="at"
          iconPosition="left"
          placeholder="Email"
          type="email"
          disabled
          value={user.email}
        />
        <br />
        <br />
        <Grid.Row>
          <Input
            icon="chat"
            iconPosition="left"
            placeholder="Set your status"
            type="text"
            disabled
            value={user.status}
            className={styles.chatInput}
          />
          <Button icon="edit" onClick={() => toggle(user.status === undefined ? '' : user.status)} />
        </Grid.Row>
      </Grid.Column>
    </Grid>
    {editedStatus !== undefined && (
      <EditStatus
        toggleEditedStatus={toggle}
        updateStatus={update}
        status={user.status === undefined ? '' : user.status}
      />
    )}
  </>
);

Profile.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  editedStatus: PropTypes.string,
  toggleEditedStatus: PropTypes.func.isRequired,
  updateStatus: PropTypes.func.isRequired
};

Profile.defaultProps = {
  user: {},
  editedStatus: undefined
};

const mapStateToProps = rootState => ({
  user: rootState.profile.user,
  editedStatus: rootState.profile.editedStatus
});

const actions = {
  toggleEditedStatus,
  updateStatus
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
