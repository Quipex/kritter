import { SET_EDITED_STATUS, SET_USER, UPDATE_STATUS } from 'src/containers/Profile/actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case SET_USER:
      return {
        ...state,
        user: action.user,
        isAuthorized: Boolean(action.user?.id),
        isLoading: false
      };
    case SET_EDITED_STATUS:
      return {
        ...state,
        editedStatus: action.status
      };
    case UPDATE_STATUS:
      return {
        ...state,
        user: {
          ...state.user,
          status: action.status
        }
      };
    default:
      return state;
  }
};
