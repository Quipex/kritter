import React from 'react';
import Logo from 'src/components/Logo';
import { Grid, Header, Message } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';
import ResetPasswordForm from 'src/components/ResetPasswordForm';
import { requestResetPassword } from 'src/containers/Profile/actions';

const RestorePasswordPage = () => (
  <Grid textAlign="center" verticalAlign="middle" className="fill">
    <Grid.Column style={{ maxWidth: 450 }}>
      <Logo />
      <Header as="h2" color="teal" textAlign="center">
        Restore a password
      </Header>
      <ResetPasswordForm restore={requestResetPassword} />
      <Message>
        Back to the login page
        {' '}
        <NavLink exact to="/login">Log In</NavLink>
      </Message>
    </Grid.Column>
  </Grid>
);

export default RestorePasswordPage;
