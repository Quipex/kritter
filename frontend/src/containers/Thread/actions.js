import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
  ADD_POST,
  DELETE_COMMENT,
  DELETE_POST,
  LOAD_MORE_POSTS,
  RATE_COMMENT,
  SET_ALL_POSTS,
  SET_EDITED_COMMENT,
  SET_EDITED_POST,
  SET_EXPANDED_POST,
  UPDATE_COMMENT,
  UPDATE_FILTER,
  UPDATE_POST
} from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

const setEditedPostAction = post => ({
  type: SET_EDITED_POST,
  post
});

const updatePostAction = post => ({
  type: UPDATE_POST,
  post
});

const updateCommentAction = comment => ({
  type: UPDATE_COMMENT,
  comment
});

const updateFilterAction = filter => ({
  type: UPDATE_FILTER,
  filter
});

const deletePostAction = deletedPostId => ({
  type: DELETE_POST,
  deletedPostId
});

const deleteCommentAction = commentId => ({
  type: DELETE_COMMENT,
  commentId
});

const rateCommentAction = comment => ({
  type: RATE_COMMENT,
  comment
});

const setEditedCommentAction = comment => ({
  type: SET_EDITED_COMMENT,
  comment
});

const getReactablesWithUserRating = (reactables, userReactions) => {
  const reactablesWithUserRating = [];
  let wasRated = false;
  reactables.forEach(reactable => {
    wasRated = false;
    userReactions.forEach(reaction => {
      if (reaction.id === reactable.id) {
        reactablesWithUserRating.push({
          ...reactable,
          isLiked: reaction.isLike === true,
          isDisliked: reaction.isLike === false
        });
        wasRated = true;
      }
    });
    if (!wasRated) {
      reactablesWithUserRating.push(reactable);
    }
  });
  return reactablesWithUserRating;
};

export const loadPosts = () => async (dispatch, getRootState) => {
  const posts = await postService.getAllPosts(getRootState().posts.postsFilter);
  const postIds = posts.map(p => p.id);
  const userPostsRatings = await postService.getUserPostsRating(postIds);
  const ratedPosts = getReactablesWithUserRating(posts, userPostsRatings);
  dispatch(setPostsAction(ratedPosts));
};

export const loadMorePosts = () => async (dispatch, getRootState) => {
  const { posts: { posts, postsFilter } } = getRootState();
  const loadedPosts = await postService.getAllPosts(postsFilter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  const filteredPostsIds = filteredPosts.map(p => p.id);
  const userPostsRatings = await postService.getUserPostsRating(filteredPostsIds);
  const filteredRatedPosts = getReactablesWithUserRating(filteredPosts, userPostsRatings);
  dispatch(addMorePostsAction(filteredRatedPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

export const deletePost = postId => async dispatch => {
  await postService.deletePost(postId);
  dispatch(deletePostAction(postId));
};

export const deleteComment = commentId => async dispatch => {
  await commentService.deleteComment(commentId);
  dispatch(deleteCommentAction(commentId));
};

export const updatePost = post => async dispatch => {
  const { id } = await postService.updatePost(post);
  const updatedPost = await postService.getPost(id);
  const userPostRating = await postService.getUserPostsRating([id]);
  dispatch(updatePostAction(getReactablesWithUserRating([updatedPost], userPostRating)[0]));
};

export const updateComment = comment => async dispatch => {
  const { id } = await commentService.updateComment(comment);
  const updatedPost = await commentService.getComment(id);
  const userPostRating = await commentService.getUserCommentsRating([id]);
  dispatch(updateCommentAction(getReactablesWithUserRating([updatedPost], userPostRating)[0]));
};

export const toggleExpandedPost = postId => async dispatch => {
  if (postId === undefined) {
    dispatch(setExpandedPostAction(undefined));
  } else {
    const post = await postService.getPost(postId);
    const userPostsRatings = await postService.getUserPostsRating([postId]);
    const userCommentsRatings = await commentService.getUserCommentsRating(post.comments.map(c => c.id));
    post.comments = getReactablesWithUserRating(post.comments, userCommentsRatings);
    dispatch(setExpandedPostAction(getReactablesWithUserRating([post], userPostsRatings)[0]));
  }
};

export const toggleEditedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setEditedPostAction(post));
};

export const toggleEditedComment = comment => async dispatch => {
  dispatch(setEditedCommentAction(comment));
};

const mapRating = (rateable, rateResult) => ({
  ...rateable,
  isLiked: rateResult.isLike === true,
  isDisliked: rateResult.isLike === false,
  likeCount: Number(rateResult.likeCount),
  dislikeCount: Number(rateResult.dislikeCount)
});

export const ratePost = (ratedPost, isLike) => async (dispatch, getRootState) => {
  const result = await postService.ratePost(ratedPost.id, isLike);

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== ratedPost.id ? post : mapRating(post, result)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === ratedPost.id) {
    dispatch(setExpandedPostAction(mapRating(expandedPost, result)));
  }
};

export const rateComment = (ratedComment, isLike) => async dispatch => {
  const rateResponse = await commentService.rateComment(ratedComment.id, isLike);

  dispatch(rateCommentAction(mapRating(ratedComment, rateResponse)));
};

export const updateFilter = filter => async dispatch => {
  dispatch(updateFilterAction(filter));
};

export const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};
