/* eslint-disable no-unused-vars */
import React, { useState } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as imageService from 'src/services/imageService';
import ExpandedPost from 'src/containers/ExpandedPost';
import Post from 'src/components/Post';
import UpdatePost from 'src/components/UpdatePost';
import SharedPostLink from 'src/components/SharedPostLink';
import { Checkbox, Loader, Menu } from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import EditedPost from 'src/containers/EditedPost';
import {
  addPost,
  deletePost,
  loadMorePosts,
  loadPosts,
  ratePost,
  toggleEditedPost,
  toggleExpandedPost,
  updateFilter
} from 'src/containers/Thread/actions';

import { toggleEditedStatus, updateStatus } from 'src/containers/Profile/actions';
import EditStatus from 'src/components/EditStatus';
import styles from './styles.module.scss';

const Thread = ({
  userId,
  // loadPosts: loadP,
  loadMorePosts: loadMore,
  posts = [],
  expandedPost,
  editedPost,
  hasMorePosts,
  addPost: createPost,
  ratePost: rate,
  postsFilter,
  updateFilter: setFilter,
  toggleExpandedPost: toggleExpanded,
  toggleEditedPost: toggleEdited,
  deletePost: deleteP,
  editedStatus,
  toggleEditedStatus: toggleStatus,
  updateStatus: updateS,
  user
}) => {
  const [sharedPostId, setSharedPostId] = useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = useState(false);
  const [hideOwnPosts, setHideOwnPosts] = useState(false);

  const setFilterAndLoad = filter => {
    setFilter(filter);
  };

  const firstPageFilter = filter => ({
    from: 0,
    count: 10,
    filter
  });

  const authorsFilter = filter => (
    firstPageFilter({ authorsFilter: filter })
  );

  const toggleShowOwnPosts = () => {
    setShowOwnPosts(!showOwnPosts);
    // 'hide own' and 'show only own' can't be enabled simultaneously
    if (hideOwnPosts) {
      setHideOwnPosts(false);
    }

    setFilterAndLoad(authorsFilter(!showOwnPosts ? 'SHOW_ONLY' : undefined));
  };

  const toggleHideOwnPosts = () => {
    setHideOwnPosts(!hideOwnPosts);
    // 'hide own' and 'show only own' can't be enabled simultaneously
    if (showOwnPosts) {
      setShowOwnPosts(false);
    }

    setFilterAndLoad(authorsFilter(!hideOwnPosts ? 'HIDE' : undefined));
  };

  const onlyLikedFilter = state => (
    firstPageFilter({ showOnlyLiked: state })
  );

  const showOnlyLiked = state => {
    setFilterAndLoad(onlyLikedFilter(state));
  };

  const getMorePosts = () => loadMore(postsFilter);

  const sharePost = id => {
    setSharedPostId(id);
  };

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <UpdatePost updatePost={createPost} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        <Menu fluid widths={2}>
          <Menu.Item
            name="All posts"
            icon="newspaper"
            active={postsFilter.filter.showOnlyLiked === false}
            onClick={() => showOnlyLiked(false)}
          />
          <Menu.Item
            name="Liked posts"
            icon="heart"
            active={postsFilter.filter.showOnlyLiked === true}
            onClick={() => showOnlyLiked(true)}
          />
        </Menu>
        <div className={styles.checkboxes}>
          <Checkbox
            toggle
            label="Show only my posts"
            checked={showOwnPosts}
            onChange={toggleShowOwnPosts}
          />
          <Checkbox
            toggle
            label="Hide my posts"
            checked={hideOwnPosts}
            onChange={toggleHideOwnPosts}
          />
        </div>
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Loader active inline="centered" key={0} />}
      >
        {posts.map(post => (
          <Post
            post={post}
            ratePost={rate}
            toggleExpandedPost={toggleExpanded}
            sharePost={sharePost}
            key={post.id}
            currentUser={userId}
            toggleEditedPost={toggleEdited}
            deletePost={deleteP}
          />
        ))}
      </InfiniteScroll>
      {expandedPost && <ExpandedPost sharePost={sharePost} />}
      {sharedPostId && <SharedPostLink postId={sharedPostId} close={() => setSharedPostId(undefined)} />}
      {editedPost && <EditedPost />}
      {editedStatus !== undefined && (
        <EditStatus
          toggleEditedStatus={toggleStatus}
          updateStatus={updateS}
          status={user.status === undefined ? '' : user.status}
        />
      )}
    </div>
  );
};

Thread.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.object),
  hasMorePosts: PropTypes.bool,
  expandedPost: PropTypes.objectOf(PropTypes.any),
  editedPost: PropTypes.objectOf(PropTypes.any),
  userId: PropTypes.string,
  loadMorePosts: PropTypes.func.isRequired,
  ratePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  toggleEditedPost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  addPost: PropTypes.func.isRequired,
  postsFilter: PropTypes.objectOf(PropTypes.any),
  updateFilter: PropTypes.func.isRequired,
  updateStatus: PropTypes.func.isRequired,
  toggleEditedStatus: PropTypes.func.isRequired,
  user: PropTypes.objectOf(PropTypes.any),
  editedStatus: PropTypes.string
};

Thread.defaultProps = {
  posts: [],
  hasMorePosts: true,
  expandedPost: undefined,
  editedPost: undefined,
  userId: undefined,
  postsFilter: {
    from: 0,
    count: 10
  },
  user: undefined,
  editedStatus: undefined
};

const mapStateToProps = rootState => ({
  posts: rootState.posts.posts,
  hasMorePosts: rootState.posts.hasMorePosts,
  expandedPost: rootState.posts.expandedPost,
  userId: rootState.profile.user.id,
  postsFilter: rootState.posts.postsFilter,
  editedPost: rootState.posts.editedPost,
  editedStatus: rootState.profile.editedStatus,
  user: rootState.profile.user
});

const actions = {
  loadPosts,
  loadMorePosts,
  ratePost,
  toggleExpandedPost,
  toggleEditedPost,
  addPost,
  deletePost,
  updateFilter,
  updateStatus,
  toggleEditedStatus
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Thread);
