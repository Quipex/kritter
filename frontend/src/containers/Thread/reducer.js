import { SET_USER } from 'src/containers/Profile/actionTypes';
import {
  ADD_POST,
  DELETE_COMMENT,
  DELETE_POST,
  LOAD_MORE_POSTS,
  RATE_COMMENT,
  SET_ALL_POSTS,
  SET_EDITED_COMMENT,
  SET_EDITED_POST,
  SET_EXPANDED_POST,
  UPDATE_COMMENT,
  UPDATE_FILTER,
  UPDATE_POST
} from './actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case SET_ALL_POSTS:
      return {
        ...state,
        posts: action.posts,
        hasMorePosts: Boolean(action.posts.length),
        // increase every time more posts are loaded
        postsFilter: {
          ...state.postsFilter,
          from: state.postsFilter.from + state.postsFilter.count
        }
      };
    case LOAD_MORE_POSTS:
      return {
        ...state,
        posts: [...(state.posts || []), ...action.posts],
        hasMorePosts: Boolean(action.posts.length),
        // increase every time more posts are loaded
        postsFilter: {
          ...state.postsFilter,
          from: state.postsFilter.from + state.postsFilter.count
        }
      };
    case ADD_POST:
      return {
        ...state,
        posts: [action.post, ...state.posts]
      };
    case SET_EXPANDED_POST:
      return {
        ...state,
        expandedPost: action.post
      };
    case SET_USER:
      return {
        ...state,
        posts: [],
        hasMorePosts: true,
        postsFilter: {
          filter: {
            showOnlyLiked: false,
            authorsPostsFilter: undefined
          },
          from: 0,
          count: 10
        }
      };
    case UPDATE_FILTER:
      return {
        ...state,
        posts: [],
        postsFilter: {
          ...state.postsFilter,
          ...action.filter,
          filter: {
            ...state.postsFilter.filter,
            ...action.filter.filter
          }
        },
        hasMorePosts: true
      };
    case SET_EDITED_POST:
      return {
        ...state,
        editedPost: action.post
      };
    case UPDATE_POST:
      return {
        ...state,
        posts: state.posts.map(post => (post.id === action.post.id ? action.post : post)),
        expandedPost: state.expandedPost !== undefined ? action.post : undefined
      };
    case UPDATE_COMMENT:
      return {
        ...state,
        expandedPost: {
          ...state.expandedPost,
          comments: [...state.expandedPost.comments.map(c => (c.id === action.comment.id ? action.comment : c))]
        }
      };
    case DELETE_POST:
      return {
        ...state,
        posts: state.posts.filter(post => post.id !== action.deletedPostId)
      };
    case DELETE_COMMENT:
      return {
        ...state,
        expandedPost: {
          ...state.expandedPost,
          comments: [...state.expandedPost.comments.filter(c => (c.id !== action.commentId))]
        }
      };
    case RATE_COMMENT:
      return {
        ...state,
        expandedPost: {
          ...state.expandedPost,
          comments: [...state.expandedPost.comments.map(c => (c.id === action.comment.id ? action.comment : c))]
        }
      };
    case SET_EDITED_COMMENT:
      return {
        ...state,
        editedComment: action.comment
      };
    default:
      return state;
  }
};
