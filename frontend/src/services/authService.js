import callWebApi from 'src/helpers/webApiHelper';

export const login = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/login',
    type: 'POST',
    request
  });
  return response.json();
};

export const registration = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/register',
    type: 'POST',
    request
  });
  return response.json();
};

export const getCurrentUser = async () => {
  try {
    const response = await callWebApi({
      endpoint: '/api/user',
      type: 'GET'
    });
    return response.json();
  } catch (e) {
    return null;
  }
};

export const requestResetPassword = async request => {
  await callWebApi({
    endpoint: '/api/auth/resetPassword',
    type: 'POST',
    request
  });
};

export const setNewPassword = async request => {
  await callWebApi({
    endpoint: '/api/auth/changePassword',
    type: 'POST',
    request
  });
};
