import callWebApi from 'src/helpers/webApiHelper';

export const addComment = async request => {
  const response = await callWebApi({
    endpoint: '/api/comments/create',
    type: 'POST',
    request
  });
  return response.json();
};

export const getComment = async id => {
  const response = await callWebApi({
    endpoint: `/api/comments/${id}`,
    type: 'GET'
  });
  return response.json();
};

export const rateComment = async (commentId, isLike) => {
  const response = await callWebApi({
    endpoint: '/api/comment_reaction',
    type: 'PUT',
    request: {
      commentId,
      isLike
    }
  });
  return response.json();
};

export const getUserCommentsRating = async commentsIds => {
  const response = await callWebApi({
    endpoint: '/api/comment_reaction',
    type: 'POST',
    request: commentsIds
  });
  return response.json();
};

export const updateComment = async request => {
  const response = await callWebApi({
    endpoint: '/api/comments/update',
    type: 'POST',
    request
  });
  return response.json();
};

export const deleteComment = async id => {
  await callWebApi({
    endpoint: `/api/comments/delete/${id}`,
    type: 'DELETE'
  });
};
