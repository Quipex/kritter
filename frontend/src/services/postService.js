import callWebApi from 'src/helpers/webApiHelper';

export const getAllPosts = async filter => {
  const response = await callWebApi({
    endpoint: '/api/posts',
    type: 'POST',
    request: filter
  });
  return response.json();
};

export const addPost = async request => {
  const response = await callWebApi({
    endpoint: '/api/posts/create',
    type: 'POST',
    request
  });
  return response.json();
};

export const getPost = async id => {
  const response = await callWebApi({
    endpoint: `/api/posts/${id}`,
    type: 'GET'
  });
  return response.json();
};

export const ratePost = async (postId, isLike) => {
  const response = await callWebApi({
    endpoint: '/api/post_reaction',
    type: 'PUT',
    request: {
      postId,
      isLike
    }
  });
  return response.json();
};

export const getUserPostsRating = async postIds => {
  const response = await callWebApi({
    endpoint: '/api/post_reaction',
    type: 'POST',
    request: postIds
  });
  return response.json();
};

export const updatePost = async request => {
  const response = await callWebApi({
    endpoint: '/api/posts/update',
    type: 'POST',
    request
  });
  return response.json();
};

export const deletePost = async id => {
  await callWebApi({
    endpoint: `/api/posts/${id}`,
    type: 'DELETE'
  });
};

// should be replaced by appropriate function
export const getPostByHash = async hash => getPost(hash);
