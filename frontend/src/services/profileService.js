import callWebApi from 'src/helpers/webApiHelper';

export const updateStatus = async newStatus => {
  const response = await callWebApi({
    endpoint: '/api/user/status',
    type: 'PUT',
    request: {
      status: newStatus
    }
  });
  return response.json();
};
